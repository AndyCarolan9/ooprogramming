/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animalproj;

/**
 *
 * @author Andy Carolan
 */
public class Pet extends Animal{
    private String Breed;
    private String OwnerName; 
    
    public Pet(String Species, boolean iswild, boolean SPT, String Sound, String breed, String ownerName){
        super(Species, iswild, SPT, Sound);
        Breed = breed;
        OwnerName = ownerName;
    }
}
