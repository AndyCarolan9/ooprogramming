package com.mycompany.animalproj;

/**
 *
 * @author Andy Carolan
 */
public class Animal {
    private String species;
    private boolean isWild;
    private boolean SharpPointyTeeth;
    private String sound;
    
    public Animal(String Species, boolean iswild, boolean SPT, String Sound){
        species = Species;
        isWild = iswild;
        SharpPointyTeeth = SPT;
        sound = Sound;
    }
    
    public boolean getSPT(){
        return SharpPointyTeeth;
    }
    
    public boolean getisWild(){
        return isWild;
    }
    
    public String getSound(){
        return sound;
    }
}
