/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Week3;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author andyc
 */
public class StudentTest {
    
    public StudentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Student.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Student instance = new Student("Joe", 0, 0);
        String expResult = "Joe";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTotalScore method, of class Student.
     */
    @Test
    public void testGetTotalScore() {
        System.out.println("getTotalScore");
        Student instance = new Student();
        instance.addTest(50);
        int expResult = 50;
        int result = instance.getTotalScore();
        assertEquals(expResult, result);
        instance.addTest(-20);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTotalQuizes method, of class Student.
     */
    @Test
    public void testGetTotalQuizes() {
        System.out.println("getTotalQuizes");
        Student instance = new Student();
        instance.addTest(59);
        instance.addTest(29);
        int expResult = 2;
        int result = instance.getTotalQuizes();
        assertEquals(expResult, result);
    }

    /**
     * Test of addTest method, of class Student.
     */
    @Test
    public void testAddTest() {
        System.out.println("addTest");
        Student instance = new Student();
        instance.addTest(50);
        instance.addTest(99);
        instance.addTest('a');
    }

    /**
     * Test of AverageScore method, of class Student.
     */
    @Test
    public void testAverageScore() {
        System.out.println("AverageScore");
        Student instance = new Student();
        instance.addTest(20);
        instance.addTest(0);
        instance.addTest(99);
        double expResult = 40;
        double result = instance.AverageScore();
        assertEquals(expResult, result, 0.0);
    }
    
}
