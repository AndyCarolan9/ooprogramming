/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ooprogramming;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andy Carolan
 */
public class VotingCounterTest {
    
    public VotingCounterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTrumpCount method, of class VotingCounter.
     */
    @Test
    public void testGetTrumpCount() {
        System.out.println("getTrumpCount");
        VotingCounter instance = new VotingCounter(0);
        instance.addTrumpCount();
        instance.addTrumpCount();
        int expResult = 2;
        int result = instance.getTrumpCount();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getHillaryCount method, of class VotingCounter.
     */
    @Test
    public void testGetHillaryCount() {
        System.out.println("getHillaryCount");
        VotingCounter instance = new VotingCounter(0);
        instance.addHillaryCount();
        instance.addHillaryCount();
        int expResult = 2;
        int result = instance.getHillaryCount();
        assertEquals(expResult, result);
    }
    
    /**
     * 
     */
    @Test
    public void testaddTrumpCount(){
        System.out.println("AddTrumpCount");
        VotingCounter instance = new VotingCounter(0);
        instance.addTrumpCount();
        int expResult = 1;
        int result = instance.getTrumpCount();
        assertEquals(expResult, result);
    }
    
    /**
     * 
     */
    @Test
    public void testaddHillaryCount(){
        System.out.println("AddHillaryCount");
        VotingCounter instance = new VotingCounter(0);
        instance.addHillaryCount();
        int expResult = 1;
        int result = instance.getHillaryCount();
        assertEquals(expResult, result);
    }
    
    /**
     * 
     */
    @Test
    public void testClearCount(){
        System.out.println("ClearCount");
        VotingCounter instance = new VotingCounter(0);
        int expResult = 0;
        int result = instance.getHillaryCount();
        instance.addHillaryCount();
        instance.ClearCount();
        assertEquals(expResult, result);
        instance.addTrumpCount();
        instance.ClearCount();
        result = instance.getTrumpCount();
        assertEquals(expResult, result);
    }
    
}
