/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ooprogramming;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author andyc
 */
public class BankAccountTest {
    
    public BankAccountTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of deposit method, of class BankAccount
     */
    @Test
    public void testDeposit()
    {
        System.out.println("deposit");
        double depositAmount = 100.0;
        BankAccount instance = new BankAccount(0.0);
        instance.deposit(depositAmount);
        double expectedResult = 100.0;
        double result = instance.getBalance();
        assertEquals(expectedResult, result, 0.0);
        instance.deposit(0.0);
        result = instance.getBalance();
        assertEquals(expectedResult, result, 0.0);
        instance.deposit(100.0);
        result = instance.getBalance();
        assertEquals(expectedResult, result, 0.0);
        instance.deposit(3.33333);
        result = instance.getBalance();
        assertEquals(expectedResult, result, 0.0);
    }
    
    /**
     * 
     */
    @Test
    public void testWithDraw()
    {
        System.out.println("withdraw");
        double wihdrawAmount = 0.0;
        BankAccount instance = new BankAccount(0.0);
        boolean expResult = false;
        boolean result = instance.withdraw(wihdrawAmount);
        assertEquals(expResult, result);
        instance.deposit(100.0);
        instance.withdraw(50.0);
        boolean expectedResult = true;
        assertEquals(expectedResult, result);
        //double expectedBalance = 50.0;
        //double actualBalance = instance.getBalance();
        //assertEquals(expectedBalance, actualBalance, 0.0);
               
        testWithDraw(instance, 51.0, 50);
        testWithDraw(instance, -100.0, 50);
        testWithDraw(instance, 0.0, 50);
        
        /*result = instance.withdraw(-100.0);
        expectedResult = false;
        assertEquals(expectedResult, result);
        expectedBalance = 50.0;
        actualBalance = instance.getBalance();
        assertEquals(expectedBalance, actualBalance, 0.0);
        
        result = instance.withdraw('a');
        expectedResult = false;
        assertEquals(expectedResult, result);
        expectedBalance = 50.0;
        actualBalance = instance.getBalance();
        assertEquals(expectedBalance, actualBalance, 0.0);*/
    }
    
    /**
     * 
     * 
     */
    @Test
    public void testgetBalance()
    {
        System.out.println("getBalance");
        BankAccount instance = new BankAccount(100);
        double expResult = 100.0;
        double result = instance.getBalance();
        assertEquals(expResult, result, 0.0);
    }
    
    /**
     * 
     */
    @Test
    public void testgetAccountNumber()
    {
        System.out.println("getAccountNumber");
        testSettingOfAccountNumber(3);
        testSettingOfAccountNumber(4);
    }
    
    public void testWithDraw(BankAccount accountToWithDrawFrom,
            Double amount, double expectedBalance)
    {
        boolean result;
        result = accountToWithDrawFrom.withdraw(51.0);
        boolean expectedResult = false;
        assertEquals(expectedResult, result);
        double actualBalance =  accountToWithDrawFrom.getBalance();
        assertEquals(expectedBalance, actualBalance, 0.0);
    }
    
    public void testSettingOfAccountNumber(int expectedAccountNumber)
    {
        BankAccount instance = new BankAccount(100);
        int result = instance.getAccountNumber();
        assertEquals(expectedAccountNumber, result);
    }
}
