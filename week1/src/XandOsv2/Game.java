/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XandOsv2;

import java.util.Scanner;
/**
 *
 * @author andyc
 */
public class Game {
    public static void main(String[] args)
    {
        UIMenu menu = new UIMenu();
        menu.StartScreen();
        int option = menu.Gamemenu();
        
        if (option == 1){
            InitializeGame();
        }
        else
        {
            System.exit(0);
        }
    }
    
    public static void InitializeGame()
    {
        GameBoardV2 Board = new GameBoardV2();
        Board.initializeBoard();
        Board.PrintGameBoard();
        PlayGame(Board);
    }
    
    public static void PlayGame(GameBoardV2 Board)
    {
        int row = 0;
        int column = 0;
        char CurrentPlayer = '_';
        int i;
        
        do
        {
            i = PickPosition(row, column, Board, CurrentPlayer);
            Board.PrintGameBoard();
        }
        while(i == 0);
    }
    
    public static int PickPosition(int row, int column, GameBoardV2 Board, char CurrentPlayer)
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input the row number you want to place your piece in [0-2]: ");
        row = sc.nextInt();
        System.out.print("Please input the column number you want to place your piece in[0-2]: ");
        column = sc.nextInt();
        System.out.print("Please enter your  X or O:");
        CurrentPlayer = sc.next().charAt(0);
        CurrentPlayer = Character.toUpperCase(CurrentPlayer);
        
        if (Board.CheckWin(row, column, CurrentPlayer))
        {
            System.out.println("Winner " + CurrentPlayer);
            int i = -1;
            return i;
        }
        else if (Board.checkInput(row, column, CurrentPlayer))
        {
            Board.SetCell(row, column, CurrentPlayer);
        }
        else
        {
            System.out.println("Invalid input.");
            System.out.println("Please input a valid input.");
        }
        return 0;
    }
}
