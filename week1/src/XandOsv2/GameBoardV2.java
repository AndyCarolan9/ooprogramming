package XandOsv2;

/**
 *
 * @author andyc
 */
public class GameBoardV2 {
    int Rows = 3;
    int Columns = 3;
    char[][] Board = new char[Rows][Columns];

    public GameBoardV2() 
    {
        this.Board = Board;
    }
    
    /**
     * 
     */
    public void initializeBoard()
    {
        for (int i = 0; i < Rows; i++)
        {
            for (int j = 0; j < Columns; j++)
            {
                Board[i][j] = '_';
            }
        }
    }
    
    /**
     * 
     */
    public void PrintGameBoard()
    {
        for (int i = 0; i < Rows; i++)
        {
            for (int j = 0; j < Columns; j++)
            {
                System.out.print(Board[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("");
    }
    
    public boolean checkInput(int Row, int Column, char Currentplayer)
    {
        if(Row == 0 || Row == 1 || Row == 2)
        {
            if(Column == 0 || Column == 1 || Column == 2)
            {
                if(Currentplayer == 'X' || Currentplayer == 'O')
                {
                    if(Board[Row][Column] == '_')
                    {
                        return true; 
                    }
                }
            }
        }      
        return false;
    }
    
    /**
     * 
     * @param Row
     * @param Column
     * @param CurrentPlayer 
     */
    public void SetCell(int Row, int Column, char CurrentPlayer)
    {
        if ((Row >= 0) && (Row<Rows))
        {
            if ((Column >=0) && (Column < Columns))
            {
                if(Board[Row][Column] == '_')
                {
                    Board[Row][Column] = CurrentPlayer;
                }
            }
        }
    }
    
    public boolean CheckWin(int Row, int Column, char CurrentPlayer)
    {
        for(int i = 0; i<Rows; i++)
        {
            // Check Rows
            if(Board[Row][i] != CurrentPlayer)
            {
              break;  
            }
            
            // Check Columns
            else if(Board[i][Column] != CurrentPlayer)
            {
                break;
            }
            
            // Check Diagonal
            else if(Board[i][i] != CurrentPlayer)
            {
                break;
            }
                      
            // Check Opposite Diagonal
            else if(Board[i][(Columns-1)-i] != CurrentPlayer)
            {
                break;
            }
            else if(i == Rows-1)
            {
                return true;
            }
        }
        return false;
    }
}
