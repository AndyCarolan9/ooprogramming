/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XandOsv2;

/**
 *
 * @author andyc
 */
import java.util.Scanner;

public class UIMenu {   
    enum Options{Start, Quit;}
    public void StartScreen()
    {
        System.out.println("/  |  /  |                                /  |       /      \\           \n" +
"$$ |  $$ |        ______   _______    ____$$ |      /$$$$$$  |  _______ \n" +
"$$  \\/$$/        /      \\ /       \\  /    $$ |      $$ |  $$ | /       |\n" +
" $$  $$<         $$$$$$  |$$$$$$$  |/$$$$$$$ |      $$ |  $$ |/$$$$$$$/ \n" +
"  $$$$  \\        /    $$ |$$ |  $$ |$$ |  $$ |      $$ |  $$ |$$      \\ \n" +
" $$ /$$  |      /$$$$$$$ |$$ |  $$ |$$ \\__$$ |      $$ \\__$$ | $$$$$$  |\n" +
"$$ |  $$ |      $$    $$ |$$ |  $$ |$$    $$ |      $$    $$/ /     $$/ \n" +
"$$/   $$/        $$$$$$$/ $$/   $$/  $$$$$$$/        $$$$$$/  $$$$$$$/  \n" +
"                                                                        \n" +
"                                                                        ");
    }
    
    public int Gamemenu()
    {
        int optionSelect = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("1. Start Game");
        System.out.println("2. Quit");
        System.out.print("Please enter choice: ");
        int input = sc.nextInt();
        Options option = Options.values()[input - 1];
        switch(option)
        {
            case Start:
                System.out.println("Starting Game.");
                optionSelect = 1;
                break;
            case Quit:
                System.out.println("Quiting application.");
                System.out.println("Goodbye.");
                optionSelect = 2;  
        }
        return optionSelect;
    }
    
}
