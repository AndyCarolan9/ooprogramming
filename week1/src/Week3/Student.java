/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Week3;

import java.util.ArrayList;

/**
 *
 * @author andyc
 */
public class Student {
    private String name;
    private int totalTestScore;
    private int totalQuizes;
    private ArrayList<Integer> TestScores = new ArrayList<Integer>();
    
    public Student()
    {
        this.name = "NoName";
        this.totalTestScore = 0;
        this.totalQuizes = 0;
    }
    
    public Student(String name, int score, int numQuiz)
    {
        this.name = name;
        this.totalTestScore = score;
        this.totalQuizes = numQuiz;
    }
    
    public String getName()
    {
        return name;
    }
    
    public int getTotalScore()
    {
        return totalTestScore;
    }
    
    public int getTotalQuizes()
    {
        return totalQuizes;
    }
    
    public void addTest(int testScore)
    {
        if (testScore >= 0)
        {
            totalTestScore += testScore;
            TestScores.add(testScore);
            totalQuizes++;
        }
    }
    
    public double AverageScore()
    {
        double AverageScore = 0;
        if (totalQuizes !=0)
        {
            AverageScore = totalTestScore/totalQuizes; 
        }
        return AverageScore;
    }
}
