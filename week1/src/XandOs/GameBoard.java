package XandOs;

/**
 *
 * @author Andy Carolan
 */
public class GameBoard {
    char CurrentPlayer;
    int Rows = 3;
    int Columns = 3;
    char[][] Board = new char[Rows][Columns];
        
    /**
     * initializes game board
     */
    public GameBoard() {
        this.Board = Board;
    }
    
    /**
     * initializes game board to blank
     * can be run to restart the game
     */
    public void initializeBoard()
    {
        for (int i = 0; i < Rows; i++)
        {
            for (int j = 0; j < Columns; j++)
            {
                Board[i][j] = '_';
            }
        }
    }
    
    /**
     * prints the game board
     */
    public void printGameBoard()
    {
        for (int i = 0; i < Rows; i++)
        {
            for (int j = 0; j < Columns; j++)
            {
                System.out.print(Board[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
    
    /**
     * Set the user defined cell to x or o
     * 
     * @param Row
     * @param Column
     * @param CurrentPlayer 
     */
    public void SetCell(int Row, int Column, char CurrentPlayer)
    {
        if ((Row >= 0) && (Row < Rows))
        {
            if ((Column >= 0) && (Column < Columns))
            {
                if(Board[Row][Column] == '_')
                {
                    Board[Row][Column] = CurrentPlayer;
                }
            }
        }
    }
    
    /**
     * Checks if the game is won
     * 
     * 
     * @param Row
     * @param Column
     * @param CurrentPlayer
     * @return 
     */
    public boolean GameWon(int Row, int Column, char CurrentPlayer)
    {
        boolean IsGameWon = false;
        if(Board[Row][0] == 'X'        // 3-in-the-row
                   && Board[Row][1] == 'X'
                   && Board[Row][2] == 'X'
              || Board[0][Column] == 'X'      // 3-in-the-column
                   && Board[1][Column] == 'X'
                   && Board[2][Column] == 'X'
              || Row == Column            // 3-in-the-diagonal
                   && Board[0][0] == 'X'
                   && Board[1][1] == 'X'
                   && Board[2][2] == 'X'
              || Row + Column == 2    // 3-in-the-opposite-diagonal
                   && Board[0][2] == 'X'
                   && Board[1][1] == 'X'
                   && Board[2][0] == 'X')
        {
            IsGameWon = true;
        }
        return IsGameWon;
    }
    
    /**
     * checks if the board is full
     * 
     * @return 
     */
    public boolean DrawGame()
    {
        boolean IsBoardFull = true;
        for (int i = 0; i < Rows; i++)
        {
            for (int j = 0; j < Rows; j++)
            {
                if (Board[i][j] == '_')
                {
                    IsBoardFull = false;
                }
            }
        }
        return IsBoardFull;
    }
}
