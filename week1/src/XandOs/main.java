package XandOs;

import java.util.Scanner;
/**
 *
 * @author Andy Carolan
 */
public class main {
    /**
     * @param args 
     * main method 
     * calls initialize game
     */
    public static void main(String[] args)
    {        
        InitializeGame();
    }
    
    /**
     * Initializes the board and game
     * Prints game board
     * Runs Plays Game
     */
    public static void InitializeGame()
    {
        GameBoard Board = new GameBoard();
        Board.initializeBoard();
        Board.printGameBoard();
        PlayGame(Board);
    }
    
    /**
     * Runs the Method pick position
     * if the game board is not full
     * or GameWon is not true
     * If GameWon is true it runs the endgame method
     * 
     * @param Board 
     */
    public static void PlayGame(GameBoard Board)
    {
        int row = 0;
        int column = 0;
        int i = 0;
        char CurrentPlayer = '_';
        boolean IsGameWon = false;
        
        do
        {
            IsGameWon = PickPosition(row, column, Board, i, CurrentPlayer);
            Board.printGameBoard();
        } while (!Board.DrawGame() || IsGameWon == true);
        if (Board.GameWon(row, column, CurrentPlayer) == true)
        {
            EndGame(CurrentPlayer);
        }
    }
    
    /**
     * Allows player to input the position of where they 
     * want to place their x or o
     * Checks weather or not the inputs are valid
     * 
     * @param RowNum
     * @param ColNum
     * @param Board
     * @param i
     * @param CurrentPlayer
     * @return IsGameWon = true || false
     */
    public static boolean PickPosition(int RowNum, int ColNum, GameBoard Board, int i, char CurrentPlayer)
    {
        Scanner sc = new Scanner(System.in);
        boolean IsGameWon = false;
        
        System.out.print("Please input the row number you want to place your piece in [0-2]: ");
        RowNum = sc.nextInt();
        System.out.print("Please input the column number you want to place your piece in[0-2]: ");
        ColNum = sc.nextInt();
        System.out.print("Please enter your  X or O:");
        CurrentPlayer = sc.next().charAt(0);
        
        boolean IsInputsOk = IsInputsOk(RowNum, ColNum, CurrentPlayer);
        boolean IsCharOk = CheckPlayer(i, CurrentPlayer);
        
        if (IsInputsOk == true /*&& IsCharOk == true*/)
        {
            IsGameWon = Board.GameWon(RowNum, ColNum, CurrentPlayer);
            Board.SetCell(RowNum, ColNum, CurrentPlayer);
            i++;
        }
        else
        {
            System.out.println("Invalid input. Please go again.");
        }
        return IsGameWon;
    }
    
    // Doesnt Work
    // TODO Fix this 
    /**
     * Checks if the player is inputing the right character
     * 
     * @param i
     * @param CurrentPlayer
     * @return 
     */
    public static boolean CheckPlayer(int i, char CurrentPlayer)
    {
        if (i == 0 || i%2 == 0 && CurrentPlayer == 'X')
        {
            return true;
        }
        else if (CurrentPlayer == 'O' && i%2 != 0)
        {
            return true;
        }
    return false;
    }
    
    /**
     * Checks if coordinates inputed are valid
     * 
     * @param RowNum
     * @param ColNum
     * @param CurrentPlayer
     * @return 
     */
    public static boolean IsInputsOk(int RowNum, int ColNum, char CurrentPlayer)
    {
        if (RowNum == 0 || RowNum == 1 || RowNum == 2 && 
            ColNum == 0 || ColNum == 1 || ColNum == 2)
        {
            return true;
        }
        return false;
    }
    
    /**
     * Prints out the winner of the game
     * 
     * @param CurrentPlayer 
     */
    public static void EndGame(char CurrentPlayer)
    {
        System.out.println(CurrentPlayer + "Won!!");
    }
}
