package Quiz;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Andy Carolan
 */
public class Demo {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        
        Question q = new Question();
        q.setText("Who was the inventor of java?");
        q.SetAnswer("James Gosling");
        
        q.display();
        System.out.print("Your answer: ");
        String response = sc.nextLine();
        System.out.println(q.checkAnswer(response));
    }
}
