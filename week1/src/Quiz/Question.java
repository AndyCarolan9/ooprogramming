package Quiz;

/**
 *
 * @author Andy Carolan
 */
public class Question {
    private String text;
    private String answer;
    
    /**
     * Constructs a question with empty question and answer.
     */
    public Question(){
        text = "";
        answer = "";
    }
    
    /**
     * set the question text
     * @param questionText the text of this question
     */
    public void setText(String questionText){
        text = questionText;
    }
    
    /**
     * Sets the answer for this question
     * @param CorrectResponse the answer
     */
    public void SetAnswer(String correctResponse){
        answer = correctResponse;
    }
    
    /**
     * checks a given response for correctness.
     * @param response the response to check
     * @return true if the response was correct, false otherwise
     */
    public boolean checkAnswer(String response){
        return response.equals(answer);
    }
    
    /**
     * Displays this question
     */
    public void display(){
        System.out.println(text);
    }

    void SetAnswer(int correctResponse) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
