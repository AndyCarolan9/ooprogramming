package Quiz;

import java.util.ArrayList;
/**
 *
 * @author Andy Carolan
 */
public class ChoiceQuestion extends Question{
    private ArrayList<String> choices;
    
    /**
     * Constructs a choice question with no choices.
     */
    public ChoiceQuestion()
    {
        choices = new ArrayList<String>();
    }
    
    /**
     * Adds an answer choice to this question.
     * @param choice the choice to add
     * @param correct true if this is the correct choice, false otherwise
     */
    public void addChoice(String choice, boolean correct){
        choices.add(choice);
        if(correct){
            String choiceString = "" + choices.size();
            //Question q = new Question();
            super.SetAnswer(choiceString);
        }
    }
    
    /**
     * 
     */
    @Override
    public void display()
    {
        super.display();
        
        for(int i = 0;i<choices.size(); i++){
            int choiceNumber = i+1;
            System.out.println(choiceNumber + ": " + choices.get(i));
        }
    }
}
