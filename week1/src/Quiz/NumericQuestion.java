package Quiz;

/**
 *
 * @author Andy Carolan
 */
public class NumericQuestion extends Question{
    private double answer;
        
    /**
     * 
     */
    public void setAnswer(double CorrectResponse){
        answer = CorrectResponse;
    }
    
    /**
     * 
     * @param response
     */
    public void CheckAnswer(double response){
         if(response == answer || response == (answer+0.01) || response == (answer-0.01)){
             super.checkAnswer(Double.toString(response));
         }
    }
}
