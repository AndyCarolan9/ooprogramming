package ooprogramming;

/**
 * Write xo's game
 * 
 * @author andyc
 */
import java.security.cert.PKIXRevocationChecker;
import java.util.Arrays;
import java.util.Scanner;
import static ooprogramming.Q6P2.printArray;

public class Gd2classWeek2 {
    enum Options{CREATE, READ , UPDATE, DELETE;}
    public static void main(String[] args)
    {
       testEnums();
        int testarray[] = {1,2,3,4,5};
        printArray(testarray);
        shallowVDeepCopyArray(testarray);
        printArray(testarray);
        printXOs();
    }
    
    public static void testEnums()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter choice:");
        int input = sc.nextInt();
        Options option = Options.values()[input];
        switch(option)
        {
            case CREATE:
                System.out.println("User wants to create");
                break;
            case READ:
                System.out.println("User wants to read");
                break;
        }
    }
    
    public static void shallowVDeepCopyArray(int[] arrayToCopy)
    {
        // Shallow array copy
        //int[] copyofArray = arrayToCopy;
        //copyofArray[0] = 100000000;
        
        //deep array copy
        int[] deepCopy = Arrays.copyOf(arrayToCopy, arrayToCopy.length);
        deepCopy[0] = 37;
        printArray(deepCopy);
    }
    
    public static void printXOs()
    {
        char[][] xoBoard = {{'X','O','O'},{'O','O','X'},{'X','X','X'}};
        for(int i = 0;i<xoBoard.length; i++)
        {
            for(int j = 0; j<xoBoard[0].length; j++)
            {
                System.out.print(xoBoard[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
