package ooprogramming;

import java.util.Random;

/**
 *
 * @author Andy Carolan
 */
public class Q6P2 
{
    public static void main(String[] args)
    {
        initialise();
    }
    
     public static void initialise()
    {
        int[] NumArray = new int[10];
        
        // Question 1
        System.out.println("Swap first and last element");
        populateArray(NumArray);
        printArray(NumArray);
        System.out.println();
        SwapFirstandLastElements(NumArray);
        System.out.println();
      
        // Quetion 2
        System.out.println("Shift elements right");
        populateArray(NumArray);
        printArray(NumArray);
        System.out.println();
        ShiftElementsRight(NumArray);
        System.out.println();
       
        // Quetion 3
        System.out.println("Replace even elements with 0");
        populateArray(NumArray);
        printArray(NumArray);
        System.out.println();
        ReplaceEvenElements(NumArray);
        System.out.println();
       
        // Quetion 4
        System.out.println("Replace each element with the larger of its neighbours");
        populateArray(NumArray);
        printArray(NumArray);
        System.out.println();
        ReplaceWithLargerNeighbour(NumArray);
        System.out.println();
        
        // Quetion 5
        System.out.println("Remove middle element");
        populateArray(NumArray);
        printArray(NumArray);
        System.out.println();
        RemoveMiddle(NumArray);
        System.out.println();
        
        // Quetion 6
        System.out.println("Move the even elements to the front of the array");
        populateArray(NumArray);
        printArray(NumArray);
        System.out.println();
        MoveEvenElements(NumArray);
        System.out.println();
       
        // Quetion 7
        System.out.println("Return Second Largest element");
        populateArray(NumArray);
        printArray(NumArray);
        System.out.println();
        int Largest = 0;
        int SecondLargest = 0;
        SecondLargest = ReturnSecondLargest(NumArray, SecondLargest, Largest);
        System.out.println(SecondLargest);
        System.out.println();
            
        // Quetion 8
        System.out.println("Return true if array is sorted");
        populateArray(NumArray);
        printArray(NumArray);
        System.out.println();
        boolean isSorted = IsArraySorted(NumArray);
        System.out.println("The Array is sorted in increasing order:" + isSorted);
        int[] SortedArray = new int[]{1,2,3,4,5,6,7};
        printArray(SortedArray);
        System.out.println();
        boolean isSorted1 = IsArraySorted(SortedArray);
        System.out.println("The Array is sorted in increasing order:" + isSorted1);
        System.out.println();
           
       // Quetion 9
        System.out.println("Return true if the array contains two adjacent duplicate elements");
        populateArray(NumArray);
        printArray(NumArray);
        System.out.println();
        boolean isAdjacentDuplicate = IsAdjacentDuplicateTrue(NumArray);
        System.out.println("The Array contains two adjacent duplicates:" + isAdjacentDuplicate);
        int[] DupArray = new int[]{1,2,3,3,5,6,7};
        printArray(DupArray);
        System.out.println();
        boolean isDuplicate = IsArraySorted(DupArray);
        System.out.println("The Array contains two adjacent duplicates:" + isDuplicate);
        System.out.println();
       
        // Quetion 10
        System.out.println("Return true if array contains duplicates");
        populateArray(NumArray);
        printArray(NumArray);
        System.out.println();
        boolean isDuplicate1 = containDuplicateElements(NumArray);
        System.out.println("The Array contains duplicates:" + isDuplicate);
        System.out.println();
        
    }
        
    public static void populateArray(int[] ArrayToPopulate)
    {
        Random rand = new Random();
        for (int i = 0; i<ArrayToPopulate.length; i++)
        {
            int x = rand.nextInt(30) + 1;
            ArrayToPopulate[i] = x;
        }
    }
    
    public static void printArray(int[] ArrayToPrint)
    {
        System.out.println("Original Array: ");
        for (int i = 0; i<ArrayToPrint.length; i++)
        {
            System.out.print(ArrayToPrint[i] + " ");
        }
        System.out.println("");
    }
    
    public static void SwapFirstandLastElements(int[] ArrayToSwap)
    {
        for (int i = 0; i<ArrayToSwap.length; i++)
        {
            if (i == 0)
            {
                System.out.print(ArrayToSwap[ArrayToSwap.length-1] + " ");
            }
            else if (i == ArrayToSwap.length-1)
            {
                System.out.print(ArrayToSwap[0]);
            }
            else
            {
                System.out.print(ArrayToSwap[i] + " ");
            }
        }
        System.out.println();
    }
    
    public static void ShiftElementsRight(int[] Array)
    {
        for (int i = 0; i<Array.length; i++)
        {
            if (i == 0)
            {
                System.out.print(Array[Array.length-1] + " ");
            }
            else
            {
                System.out.print(Array[i-1] + " ");
            }
        }
        System.out.println();
    }
    
    public static void ReplaceEvenElements(int[] ArrayToReplaceEvens)
    {
        for (int i = 0; i<ArrayToReplaceEvens.length; i++)
        {
            if (ArrayToReplaceEvens[i]%2 == 0)
            {
                System.out.print("0" + " ");
            }else 
            {
                System.out.print(ArrayToReplaceEvens[i] + " ");
            }
        }
        System.out.println();
    }
    
    public static void ReplaceWithLargerNeighbour(int[] ArrayLargerNeighbour)
    {
        for (int i = 0; i<ArrayLargerNeighbour.length; i++)
        {
            if (i == 0 || i == ArrayLargerNeighbour.length-1)
            {
                System.out.print(ArrayLargerNeighbour[i] + " ");
            }
            else if (ArrayLargerNeighbour[i-1]>ArrayLargerNeighbour[i+1])
            {
                System.out.print(ArrayLargerNeighbour[i-1] + " ");
            }
            else
            {
                System.out.print(ArrayLargerNeighbour[i+1] + " ");
            }
        }
    }
    
    public static void RemoveMiddle(int[] RemoveMiddle)
    {
        int index = 1;
        if (RemoveMiddle.length%2 == 0)
        {
            for (int i = 0; i<RemoveMiddle.length; i++)
            {
                if (index * 2 == RemoveMiddle.length){}
                else if ((index-1) * 2 == RemoveMiddle.length){}
                else
                {
                    System.out.print(RemoveMiddle[i] + " ");
                }
                index++;
            }
        }
        else
        {
            for (int i = 0; i<RemoveMiddle.length; i++)
            {
                if (index * 2 == RemoveMiddle.length+1){}
                else
                {
                    System.out.print(RemoveMiddle[i] + " ");
                }
                index++;
            }
        }
    } 
    
    public static void MoveEvenElements(int[] MoveElements)
    {
        for (int i = 0; i<MoveElements.length; i++)
        {
                if (MoveElements[i]%2 == 0)
                {
                    System.out.print(MoveElements[i] + " ");
                }
        }
        for (int i = 0; i<MoveElements.length; i++)
        {
                if (MoveElements[i]%2 != 0)
                {
                    System.out.print(MoveElements[i] + " ");
                }
        }
    }
    
    public static int ReturnSecondLargest(int[] ReturnSecondLargest, int SecondLargest, int Largest)
    {
        for(int i = 0; i<ReturnSecondLargest.length; i++)
        {
            if (ReturnSecondLargest[i] > Largest)
            {
                SecondLargest = Largest;
                Largest = ReturnSecondLargest[i];
            }
            else if (ReturnSecondLargest[i] > SecondLargest && ReturnSecondLargest[i] != Largest)
            {
                SecondLargest = ReturnSecondLargest[i];
            }
        }
        return SecondLargest;
    }
    
    public static boolean IsArraySorted(int[] IsSortedArray)
    {
        if(IsSortedArray == null)
        {
            return false;
        }
         for (int i = 0; i < IsSortedArray.length-1; i++) 
        {
            if(IsSortedArray[i] > IsSortedArray[i+1]) 
            {
                return false;
            }   
        }
        return true;
    }
    
    public static boolean IsAdjacentDuplicateTrue(int[] IsSortedArray){
        for (int i = 0; i < IsSortedArray.length - 1; i++) 
        {
            if (IsSortedArray[i] == IsSortedArray[i + 1]) 
            {
                return true;
            }
        }
        return false;
    }
    
     public static boolean containDuplicateElements(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (i != j && array[i] == array[j]) {
                    return true;
                }
            }
        }

        return false;
    }
}
