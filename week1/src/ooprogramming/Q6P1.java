package ooprogramming;

import java.util.Random;

/**
 *
 * @author andyc
 */
public class Q6P1 {
    public static void main(String[] args)
    {
        initializeArray();
    }
   
    public static void initializeArray()
    {
        int[] randArray = new int[10];
        populateArray(randArray);
        printArray(randArray);
        printArrayAtEvenindex(randArray);
        printArrayEvenElement(randArray);
        printArrayReverse(randArray);
        System.out.print("The first number is: " + randArray[0] + " ");
        System.out.print("The Last number is: " + randArray[9] + " ");
    }
    
    public static void populateArray(int[] ArraytoPopulate)
    {
        Random rand = new Random();
        for (int i = 0; i<ArraytoPopulate.length; i++)
        {
            int x = rand.nextInt(50) + 1;
            ArraytoPopulate[i] = x;
        }
    }
    
    public static void printArray(int[] ArrayToPrint)
    {
        System.out.println("Array: ");
        for (int i = 0; i<ArrayToPrint.length; i++)
        {
            System.out.print(ArrayToPrint[i] + " ");
        }
        System.out.println("");
    }
    
    public static void printArrayAtEvenindex(int[] ArrayToPrint)
    {
        for (int i = 0; i<ArrayToPrint.length; i++)
        {
            if (i%2 == 0 && i!=0)
            {
                System.out.print(ArrayToPrint[i] + " ");
            }
        }
        System.out.println("");
    }
    
    public static void printArrayEvenElement(int[] ArrayToPrint)
    {
        for (int i =0; i<ArrayToPrint.length; i++)
        {
            if (ArrayToPrint[i]%2 == 0)
            {
                System.out.print(ArrayToPrint[i] + " ");
            }
        }
        System.out.println("");
    }
    
    public static void printArrayReverse(int[] ArrayToPrint)
    {
        for (int i = ArrayToPrint.length -1 ; i>=0; i--)
        {
            System.out.print(ArrayToPrint[i] + " ");
        }
        System.out.println("");
    }
}
