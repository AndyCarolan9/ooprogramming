/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ooprogramming;

/**
 *
 * @author andyc
 */
public class HelloWorld {

    public static void main(String[] args) 
    {
        //testArray();
        //testBitShifting();
        
       /* int a = 100;
        int b = 50;
        trytoswaptwoints(a, b);
        
        int[] testArray = {1, 2, 3, 4, 5, 6, 7};
        printArray(testArray);
        swapfirstandlastIntInArray(testArray);
        printArray(testArray);
        
        // int largest = findlargestInt(testArray);
        //System.out.println("largest " + largest);
        
        //int sum = findsumInt(testArray);
        //System.out.println("sum " + sum);
        
        int [] emptyarray = {};
        // int sum1 = findlargestInt(emptyarray);
        //System.out.println("sum " + sum1);*/
        
    }

    public static void testArray() 
    {
        int[] gameScores = new int[100];
        printArray(gameScores);
    }

    public static void printArray(int[] ArrayToPrint) 
    {
        for (int i = 0; i < ArrayToPrint.length; i++) 
        {
            if ((i + 1) % 30 == 0) 
            {
                System.out.print(ArrayToPrint[i] + "\n");
            } else 
            {
                System.out.print(ArrayToPrint[i] + " ");
            }
        }
        System.out.print("\n");
    }

    public static void testBitShifting() 
    {
        int a = 2;
        a = a << 2;
        System.out.println(a);
    }

    //Primitive types are passed by value
    // the function gets a copy of the values, not the values
    // this means that the function can do whatever it wants
    // but once it returns those copies are destroyed
    // and the original values are as they were
    public static void trytoswaptwoints(int a, int b) 
    {
        int temp = a;
        a = b;
        b = temp;
    }

    public static void swapfirstandlastIntInArray(int[] testArray) 
    {
        int temp = testArray[0];
        testArray[0] = testArray[testArray.length - 1];
        testArray[testArray.length - 1] = temp;
    }

    public static int findlargestInt(int[] testArray) 
    {
        if (testArray.length == 0)
        {
            return 0;
        }
        else
        {
            int largest = testArray[0];
            for (int value : testArray) 
            {
                if (value > largest) 
                {
                    largest = value;
                }
            }
            return largest;
        }
    }
   
   /* public static int findsumInt(int[] testArray)
    {
        int total = 0;
        for (int value : testArray) 
        {
            total += testArray[value];
        }
        return total;
    }*/
}
