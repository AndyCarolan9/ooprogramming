/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ooprogramming;

/**
 *
 * @author andyc
 */
public class Tester {
    public static void main(String[] args)
    {
        BankAccount accountOne = new BankAccount(100);
        BankAccount accountTwo = accountOne;
        accountTwo.deposit(500.0);
        System.out.println("Account one: " + accountOne.getBalance());
        
        valueOrRef(accountOne);
        
        System.out.println("account one: " + accountOne.getBalance());
    }
    
    public static void valueOrRef(BankAccount acc)
    {
        acc.deposit(1_000_000.0);
    }
}
