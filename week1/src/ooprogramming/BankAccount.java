/*
Bank Account class
*/
package ooprogramming;

/**
 *
 * @author Andrew Carolan
 */
public class BankAccount 
{
    private double balance;
    private int accountNumber;
    // This is shared by all bank accounts => static
    private static int LastAccountNumber = 0;
  
    public BankAccount(double initialBalance)
    {
        this.balance = initialBalance;
        this.accountNumber = ++LastAccountNumber;
    }
    
    /**
     * Puts money into the Account
     * @param depositAmount 
     */
    public void deposit(Double depositAmount)
    {
        int decimalPlaces = getDecimalPlaces(depositAmount);
        if (depositAmount > 0 && decimalPlaces <= 2)
        {
        balance += depositAmount;
        }
    }
    
    /**
     * Finds the number of decimal places in a double
     * @param number 
     * @return 
     */
    public int getDecimalPlaces(double number)
    {
        String text = Double.toString(Math.abs(number));
        int integerPlaces = text.indexOf('.');
        int decimalPlaces = text.length() - integerPlaces - 1;
        return decimalPlaces;
    }
    
    /**
     * Takes money from account if possible
     * @param WithdrawAmount
     * @return 
     */
    public boolean withdraw(double WithdrawAmount)
    {
        boolean proceed = false;
        if (WithdrawAmount < this.getBalance())
        {
            proceed = true;
        }
        if (WithdrawAmount > 0 && proceed)
        {
        balance -= WithdrawAmount;
        return true;
        }
        else
        {
            return false;
        }
            
    }
    
    /**
     * Returns current balance
     * @return 
     */
    public double getBalance()
    {
        return balance;
    }
    
    /**
     * Returns Account number
     * @return 
     */
    public int getAccountNumber()
    {
        return accountNumber;
    }
}
