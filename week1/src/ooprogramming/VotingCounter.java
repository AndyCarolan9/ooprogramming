/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ooprogramming;

/**
 *
 * @author Andy Carolan
 */
public class VotingCounter {
    private int TrumpCount;
    private int HillaryCount;
    
    public VotingCounter(int intiialCount)
    {
        this.TrumpCount = intiialCount;
        this.HillaryCount = intiialCount;
    }
    
    /**
     * Returns the number of votes for trump
     * @return TrumpCount
     */
    public int getTrumpCount()
    {
        return TrumpCount;
    }
    
    /**
     * Returns the number of votes for trump
     * @return HillaryCount
     */
    public int getHillaryCount()
    {
        return HillaryCount;
    }
    
    /**
     * Increments trumpCount by 1
     */
    public void addTrumpCount()
    {
        TrumpCount++;
    }
    
    public void addHillaryCount()
    {
        HillaryCount++;
    }
    
    public void ClearCount()
    {
        TrumpCount = 0;
        HillaryCount = 0;
    }
}
