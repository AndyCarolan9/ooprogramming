/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ooprogramming;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andyc
 */
public class BestCustomerdisplay {
    public static void main(String[] args)
    {
        ArrayList<String> customers = new ArrayList<>();
        ArrayList<Double> sales = new ArrayList<>();
        getCustomersAndSales(customers, sales);
        System.out.println(customers);
        System.out.println(sales);
        System.out.println(nameOfBestCustomer(customers, sales) + " ");
    }
    
    public static void getCustomersAndSales(ArrayList<String> customers,
            ArrayList<Double> sales)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter all customer names, "
        + "enter 0 to indicate you are finished");
        String name = sc.next();
        while(!name.equals("0"))
        {
            customers.add(name);
            name = sc.next();
        }
        
        System.out.println("Please enter all sale amounts");
        double sale = sc.nextDouble();
        for (int i =0; i<customers.size(); i++)
        {
            System.out.println("Please enter all sales for"
            + customers.get(i) + " :");
            sales.add(sale);
            sale = sc.nextDouble();
        }
    }
    
    public static String nameOfBestCustomer(ArrayList<String> customers,
            ArrayList<Double> sales)
    {
        double highestSpend = findHighestSpend(sales);
        ArrayList<Integer> bigSpenderIndex = findBigSpenders(sales, highestSpend);
        String bigSpenders = "";
        for(int i = 0; i<bigSpenderIndex.size(); i++)
        {
            bigSpenders += customers.get(bigSpenderIndex.get(i));
        }
        return bigSpenders;
    }
    
    public static double findHighestSpend(ArrayList<Double> sales)
    {
        double highestSpend = 0;
        for(double sale : sales)
        {
            if(sale > highestSpend)
            {
                highestSpend = sale;
            }
        }
        return highestSpend;
    }
    
    public static ArrayList<Integer> findBigSpenders(ArrayList<Double> sales, double highestSpend)
    {
        ArrayList<Integer> bigSpenderIndex = new ArrayList<>();
        for(int i = 0; i < sales.size(); i++)
        {
            if(sales.get(i) == highestSpend)
            {
                bigSpenderIndex.add(i);
            }
        }
        return bigSpenderIndex;
    }
}

